//
//  ViewController.swift
//  GitRepo
//
//  Created by apple on 7/30/19.
//  Copyright © 2019 shivaapple. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    var repositories  = [RepositoryDetails]()
    var filteredRepositories = [RepositoryDetails]()
    var gitRepositoriesService = GitApiService()
    let search = UISearchController(searchResultsController: nil)
    
    @IBOutlet weak var homeTableView: UITableView!
    @IBOutlet weak var homeActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var filterButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func filterButtonClicked(_ sender: UIButton) {
        search.dismiss(animated: true, completion: nil)
    }
    
    func initialSetup() {
        
        homeTableView.delegate = self
        homeTableView.dataSource = self
        addSearchBar()
       
    }
    
    func showRepositoryTable() {
        if repositories.count > 0 {
            homeTableView.isHidden = false
            filterButton.isHidden = false
        }else {
            showMessage(message: "No repositories found")
            homeTableView.isHidden = true
        }
        self.homeActivityIndicator.stopAnimating()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch segue.identifier {
        case "showRepositoryDetail":
            if let indexPath = self.homeTableView.indexPathForSelectedRow {
                let controller = segue.destination as! RepositoryDetailViewController
                controller.repositoryDetails = repositories[indexPath.row]
            }
        case "showFilter":
            let controller = segue.destination as! FilterViewController
            controller.delegate =  self
        default:
            break
        }
    }
    
}

extension HomeViewController : UISearchControllerDelegate , UISearchBarDelegate {
    
    func addSearchBar() {
        search.obscuresBackgroundDuringPresentation = false
        search.searchBar.placeholder = "Search for repositories"
        search.searchBar.delegate = self
        search.becomeFirstResponder()
        navigationItem.searchController = search
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        if let searchText = searchBar.text , searchText.count > 0 {
            homeActivityIndicator.startAnimating()
            fetchRepositories(with: searchText)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        homeTableView.isHidden = true
        filterButton.isHidden = true
    }
    
    
}

extension HomeViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredRepositories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "homeCell", for: indexPath) as! HomeTableViewCell
        let repository = filteredRepositories[indexPath.row]
        cell.configureCell(detail: repository)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       performSegue(withIdentifier: "showRepositoryDetail", sender: self)
        
    }
    
}


extension HomeViewController : FilterDelegate {
    
    func filterBy(filter: String) {
        
        switch filter {
        case "mostStars":
             self.filteredRepositories.sort{ $0.OpenIssues > $1.OpenIssues}
        case "fewestStars":
              self.filteredRepositories.sort{ $0.OpenIssues < $1.OpenIssues}
        case "mostOpenIssues":
              self.filteredRepositories.sort{ $0.forksCount > $1.forksCount}
        case "fewestOpenIssues":
              self.filteredRepositories.sort{ $0.forksCount < $1.forksCount}
        default:
            break
        }
        homeTableView.reloadData()
     }
    
}

extension HomeViewController {
    
    func fetchRepositories(with text: String) {
        gitRepositoriesService.findRepositories(matching: text) { (repositories) in
            if let repositories = repositories {
                self.repositories = repositories.items
                self.repositories.sort { $0.watchersCount >  $1.watchersCount }
                self.filteredRepositories = self.repositories
            }else {
                self.showMessage(message: "No Repositories found")
            }
            self.homeTableView.reloadData()
            self.showRepositoryTable()
        }
    }
}
