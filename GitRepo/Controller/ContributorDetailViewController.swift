//
//  ContributorDetailViewController.swift
//  GitRepo
//
//  Created by apple on 7/31/19.
//  Copyright © 2019 shivaapple. All rights reserved.
//

import UIKit


protocol ShowRepositoryDetailDelegate {
    func showRepositoryDetail(repositoryDetail : RepositoryDetails)
}

class ContributorDetailViewController: UIViewController {
    
    var repositoryList = [RepositoryDetails]()
    var contributor: Contributor!
    var delegate: ShowRepositoryDetailDelegate!
    
    @IBOutlet weak var contributorImageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var contributorTableView: UITableView!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        contributorTableViewSetup()
        setupContributorUI()
        
    }
    
    func contributorTableViewSetup() {
        contributorTableView.delegate = self
        contributorTableView.dataSource = self
    }
    
    func setupContributorUI() {
        contributorImageView.sd_setImage(with: URL(string: contributor.avatarURL), completed: nil)
        fetchRepositoriesList()
    }
    
    func fetchRepositoriesList() {
        activityIndicator.startAnimating()
        getRepositoriesList(url: contributor.repositoryListURL) { (repositories) in
            if let repositories = repositories {
                self.repositoryList = repositories
                self.contributorTableView.reloadData()
                self.activityIndicator.stopAnimating()
            }else {
               self.showMessage(message: "No repositories found")
            }
        }
    }
    
}

extension ContributorDetailViewController : UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositoryList.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Repositotry List"
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contributorCell", for: indexPath) as! ContributorTableViewCell
        let repository = repositoryList[indexPath.row]
        cell.configureCell(detail: repository)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let repositoryDetail = repositoryList[indexPath.row]
        delegate.showRepositoryDetail(repositoryDetail: repositoryDetail)
        self.navigationController?.popViewController(animated: true)
       
    }
    
}

extension ContributorDetailViewController {
    
    func getRepositoriesList(url: String, completion: @escaping ([RepositoryDetails]?) -> ()) {
        
        DispatchQueue.global(qos: .userInteractive).async {
            guard let url = URL(string: url) else { return }
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    return
                }
                guard let data = data else { return }
                DispatchQueue.main.async {
                    let repositories = self.parse(data: data)
                    completion(repositories)
                }
            }
            task.resume()
        }
        
    }
    
    func parse(data: Data) -> [RepositoryDetails]? {
        
        let jsonDecoder = JSONDecoder()
        
        do {
            let repositories = try jsonDecoder.decode([RepositoryDetails].self, from: data)
            return repositories
        }catch {
            return nil
        }
    }
    
}
