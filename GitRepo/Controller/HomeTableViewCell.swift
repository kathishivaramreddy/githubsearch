//
//  HomeTableViewCell.swift
//  GitProfile
//
//  Created by apple on 7/30/19.
//  Copyright © 2019 shivaapple. All rights reserved.
//

import UIKit
import SDWebImage

class HomeTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var avtarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var watcherCountLabel: UILabel!
    @IBOutlet weak var starCountLabel: UILabel!
    
    func configureCell(detail : RepositoryDetails) {
        nameLabel.text = detail.name
        fullNameLabel.text = detail.fullName
        watcherCountLabel.text = "\(detail.watchersCount)"
        starCountLabel.text = "\(detail.OpenIssues)"
        avtarImageView.sd_setImage(with: URL(string: detail.ownerDetail.avatar), completed: nil)
    }
    
}
