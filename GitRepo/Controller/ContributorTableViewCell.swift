//
//  ContributorTableViewCell.swift
//  GitRepo
//
//  Created by apple on 7/31/19.
//  Copyright © 2019 shivaapple. All rights reserved.
//

import UIKit

class ContributorTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var watcherCountLabel: UILabel!
    @IBOutlet weak var forksCount: UILabel!
    
    func configureCell(detail : RepositoryDetails) {
        nameLabel.text = detail.name
        watcherCountLabel.text = "\(detail.watchersCount)"
        forksCount.text = "\(detail.forksCount)"
    }
    
}
