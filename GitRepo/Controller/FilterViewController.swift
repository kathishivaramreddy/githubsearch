//
//  FilterViewController.swift
//  GitRepo
//
//  Created by apple on 8/1/19.
//  Copyright © 2019 shivaapple. All rights reserved.
//

import UIKit


protocol FilterDelegate {
    func filterBy(filter: String)
}

class FilterViewController: UIViewController {
    
    @IBOutlet var filterView: UIView! {
        
        didSet {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
            tapGesture.numberOfTapsRequired = 1
            filterView.addGestureRecognizer(tapGesture)
        }
    }
    
    var delegate: FilterDelegate!
    
    func filterByValue(by filterValue : Int){
        
        switch filterValue {
        case 1:
            delegate.filterBy(filter: "mostStars")
        case 2:
            delegate.filterBy(filter: "fewestStars")
        case 3:
            delegate.filterBy(filter: "mostOpenIssues")
        case 4:
            delegate.filterBy(filter: "fewestOpenIssues")
        default:
            break
        }
        
    }
    
    @IBAction func filterButtonPressed(_ sender: UIButton) {
        
        filterByValue(by:sender.tag)
        dismiss(animated: true, completion: nil)
        
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }

}



