//
//  RepositoryDetailViewController.swift
//  GitRepo
//
//  Created by apple on 7/31/19.
//  Copyright © 2019 shivaapple. All rights reserved.
//

import UIKit
import WebKit
import SDWebImage

class RepositoryDetailViewController: UIViewController {

    
    var contributors = [Contributor]()
    var repositoryDetails: RepositoryDetails!
    var webView: WKWebView!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var namelabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var projectLinkLabel: UILabel! {
        didSet {
            addTapGesture()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        setupRepositortyDetailUI()
    }
    
    func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func addTapGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapForWebView(sender:)))
        tapGesture.numberOfTapsRequired = 1
        projectLinkLabel.addGestureRecognizer(tapGesture)
        projectLinkLabel.isUserInteractionEnabled = true
    }
    
    @objc func handleTapForWebView(sender: UITapGestureRecognizer) {
        
        intialSetup()
        loadWebpage()
        
    }
    
    func setupRepositortyDetailUI() {
        
        namelabel.text = repositoryDetails.name
        projectLinkLabel.text = repositoryDetails.projectLink
        descriptionLabel.text = repositoryDetails.description ?? "No Description"
        imageView.sd_setImage(with: URL(string: repositoryDetails.ownerDetail.avatar), completed: nil)
        activityIndicator.startAnimating()
        fetchContributors { (contributors) in
            if let contributors = contributors {
                self.contributors = contributors
                self.collectionView.reloadData()
            }else {
                self.showMessage(message: "No Contributors")
            }
            self.activityIndicator.stopAnimating()
        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch segue.identifier {
        case "showContributorDetail":
            if let indexPath =  self.collectionView.indexPathsForSelectedItems {
                let controller = segue.destination as! ContributorDetailViewController
                controller.delegate = self
                controller.contributor = contributors[indexPath[0].row]
            }
        default:
            break
        }
    }
    
}

extension RepositoryDetailViewController : UICollectionViewDelegate , UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return contributors.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "repositoryCell", for: indexPath) as! RepositortyCollectionViewCell
        cell.contributorImageView.sd_setImage(with: URL(string: contributors[indexPath.row].avatarURL), completed: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showContributorDetail", sender: self)
    }
}

extension RepositoryDetailViewController: WKNavigationDelegate {
    
    func intialSetup() {
        
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }
    
    func loadWebpage() {
        let url = URL(string: projectLinkLabel.text!)!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
    }
    
}

extension RepositoryDetailViewController: ShowRepositoryDetailDelegate {
    func showRepositoryDetail(repositoryDetail: RepositoryDetails) {
       self.repositoryDetails = repositoryDetail
        setupRepositortyDetailUI()
    }
}

extension RepositoryDetailViewController {
    
    func fetchContributors(completion: @escaping ([Contributor]?) -> ()) {
        
        DispatchQueue.global(qos: .userInteractive).async {
            guard let url = URL(string: self.repositoryDetails.contributors) else { return }
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    self.showMessage(message: "Network error try again")
                    return
                }
                guard let data = data else { return }
                DispatchQueue.main.async {
                    let contributors = self.parse(data: data)
                    completion(contributors)
                }
            }
            task.resume()
        }
        
    }
    
    func parse(data: Data) -> [Contributor]? {
        
        let jsonDecoder = JSONDecoder()
        do {
            let contributors = try jsonDecoder.decode([Contributor].self, from: data)
            return contributors
        }catch {
            return nil
        }
    }
    
}
