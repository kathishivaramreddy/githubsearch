//
//  GitApiService.swift
//  GitRepo
//
//  Created by apple on 7/31/19.
//  Copyright © 2019 shivaapple. All rights reserved.
//

import Foundation

struct GitApiService {
    
    func findRepositories(matching: String, completion: @escaping (Repositories?) -> ()) {
        
        DispatchQueue.global(qos: .userInteractive).async {
            guard let url = self.constructUrl(query: matching) else { return }
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    return
                }
                guard let data = data else { return }
                DispatchQueue.main.async {
                let repositories = self.parse(data: data)
                completion(repositories)
                }
            }
            task.resume()
        }
        
    }
    
    func parse(data: Data) -> Repositories? {
        
        let jsonDecoder = JSONDecoder()
        
        do {
            let repositories = try jsonDecoder.decode(Repositories.self, from: data)
            return repositories
        }catch {
            return nil
        }
        
    }
    
    func constructUrl(query: String) -> URL? {
        let query = query.trimmingCharacters(in: .whitespacesAndNewlines).replacingOccurrences(of: " ", with: "+")
        let api = "https://api.github.com"
        let endpoint = "/search/repositories?q=\(query)&per_page=10"
        let url = URL(string: api + endpoint)
        return url
    }
    
}
