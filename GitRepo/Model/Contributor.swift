//
//  Contributor.swift
//  GitRepo
//
//  Created by apple on 7/31/19.
//  Copyright © 2019 shivaapple. All rights reserved.
//

import Foundation

struct Contributor: Codable {
    
    let avatarURL: String
    let repositoryListURL: String
    
    enum CodingKeys: String, CodingKey {
        case avatarURL = "avatar_url"
        case repositoryListURL = "repos_url"
    }
}
