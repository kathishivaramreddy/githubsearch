//
//  RepositoryDetails.swift
//  GitRepo
//
//  Created by apple on 7/31/19.
//  Copyright © 2019 shivaapple. All rights reserved.
//

import Foundation

struct RepositoryDetails: Codable {
    
    let name: String
    let fullName: String
    let watchersCount: Int
    let OpenIssues: Int
    let forksCount: Int
    let projectLink: String
    let description: String?
    let contributors: String
    let ownerDetail: OwnerDetail
    
    enum CodingKeys : String, CodingKey {
        case name
        case fullName = "full_name"
        case watchersCount = "watchers_count"
        case OpenIssues = "open_issues_count"
        case forksCount = "forks_count"
        case projectLink = "html_url"
        case description
        case contributors = "contributors_url"
        case ownerDetail = "owner"
    }
    
}
