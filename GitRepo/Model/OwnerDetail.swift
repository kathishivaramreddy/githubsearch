//
//  OwnerDetail.swift
//  GitRepo
//
//  Created by apple on 7/31/19.
//  Copyright © 2019 shivaapple. All rights reserved.
//

import Foundation

struct OwnerDetail : Codable{
    
    let avatar: String
    
    enum CodingKeys: String, CodingKey {
        case avatar = "avatar_url"
    }
}
