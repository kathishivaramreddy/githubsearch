//
//  Repositories.swift
//  GitRepo
//
//  Created by apple on 7/30/19.
//  Copyright © 2019 shivaapple. All rights reserved.
//

import Foundation

struct Repositories: Codable {
    
    let items: [RepositoryDetails]
}
