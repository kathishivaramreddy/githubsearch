//
//  Util.swift
//  GitRepo
//
//  Created by apple on 8/2/19.
//  Copyright © 2019 shivaapple. All rights reserved.
//


import UIKit

extension UIViewController {
    
    func showMessage(message: String) {
        
        let alert = UIAlertController(title: "Message", message: message , preferredStyle: .alert)
        let action = UIAlertAction(title: "ok", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
}

